from django.forms import ModelForm,Textarea,EmailInput,URLInput,ValidationError
from django.utils.translation import gettext as _
from .models import Comment
import mistune

class CommentForm(ModelForm):
    class Meta:
        model  = Comment
        fields = ('nickname','website','email','content')
        labels = {
            'nickname': _('昵称'),
            'content': _('内容'),
            'email': _('邮箱'),
            'website' : _('网站'),
        }
        widgets = {
            'nickname' :Textarea(attrs = {'cols':80, 'rows':20}),
            'content' : Textarea(attrs = {'rows' : 6, 'rows' : 60}),
            'email' : EmailInput(attrs = {'class': 'form-control', 'style': "width: 60%;"}),
            'website' : URLInput(attrs = {'class': 'form-control', 'style' : "width: 60%"})
        }
    def clean_content(self):
        content = self.cleaned_data.get('content')

        if len(content) < 10:
            raise ValidationError('内容太短了')
        content = mistune.markdown(content)
        return content

